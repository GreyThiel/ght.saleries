//Grey Thiel
//Saleries Assignment

#include<iostream>
#include<conio.h>
#include<string>

using namespace std;

//Struct for employees
struct Employee
{
	int id = 0;
	string firstName = "";
	string lastName = "";
	double payRate = 0;
	double hours = 0;
};

//function skeletons (I forgot what these are actually called.)
double TotalGrossPay(Employee* array, int size);

int main()
{
	int employeeCount;

	cout << "How many employees do you have? ";
	cin >> employeeCount;
	
	Employee* employees = new Employee[employeeCount];

	for (int i = 0; i < employeeCount; i++)
	{
		cout << '\n' << "Employee ID: ";
		cin >> employees[i].id;

		cout << '\n' << "First Name: ";
		cin >> employees[i].firstName;

		cout << '\n' << "Last Name: ";
		cin >> employees[i].lastName;

		cout << '\n' << "Pay Rate: ";
		cin >> employees[i].payRate;

		cout << '\n' << "Hours: ";
		cin >> employees[i].hours;
	}

	cout << '\n';
	
	for (int i = 0; i < employeeCount; i++)
	{
		double grossPay = 0;

		cout << employees[i].id << ' ' << employees[i].firstName << ' ' << employees[i].lastName;

		grossPay = employees[i].payRate * employees[i].hours;

		cout << ' ' << grossPay << '\n';
	}

	cout << '\n' << "Total Gross Pay: " << TotalGrossPay(employees, employeeCount);

	delete[] employees;
	
	(void)_getch();
	return 0;
}

double TotalGrossPay(Employee* array, int size)
{
	double total = 0;
	
	for (int i = 0; i < size; i++)
	{
		total += array->payRate * array->hours;
	}

	return total;
}
